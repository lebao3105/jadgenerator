unit General;
{ Stores variables collected from user input and
  program's own things. }

{$mode objfpc}{$h+}

interface

uses Sysutils;

const
    progVer: string = '1.0.0';
    copyright: String = '(C) 2024 lebao3105.';
    // a complete (no fileset here as it requires a children)
    ArrayOfAttributes: TStringArray = ('name', 'description', 'version', 'vendor', 'copyright');
    // nothing more for now

var
    inputFiles: TStringArray;
    outputPath: String;
    targetProgVer: String;
    targetProgName: String;
    targetProgDes: String;
    targetProgVendor: String;

function Capitalize(const S: String): String;

implementation

function Capitalize(const S: String): String;
begin
    Result := LowerCase(s);
    Result[1] := UpCase(Result[1]);
end;

end.