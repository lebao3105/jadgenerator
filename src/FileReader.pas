unit FileReader;

{$mode objfpc}{$h+}

interface

uses DOM, XMLRead, General;

procedure ReadALX(const filePath: String);
procedure ReadJAD(const filePath: String);

implementation

procedure ReadALX(const filePath: String);
var
    ApplicationNode: TDOMNode;
    Doc: TXMLDocument;
    i: Integer;

begin
    try
        ReadXMLFile(Doc, filePath);
        ApplicationNode := Doc.DocumentElement.FindNode('application');
        for i := Low(ArrayOfAttributes) to High(ArrayOfAttributes) do
            writeln(Capitalize(ArrayOfAttributes[i]) + ': ' +
                    ApplicationNode.FindNode(ArrayOfAttributes[i]).TextContent);
        // Fileset
        writeln('Files: ');
        writeln(ApplicationNode.FindNode('fileset').FindNode('files').TextContent);
    finally
        Doc.Free;
    end;
end;

procedure ReadJAD(const filePath: String);
begin
end;

end.
