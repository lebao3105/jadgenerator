unit FileGen;

{$mode objfpc}{$h+}

interface

uses
    DOM, XMLWrite, General, Sysutils;

procedure WriteNewALX;
procedure WriteNewJAD;

implementation

procedure WriteNewALX;
var
    Doc: TXMLDocument;
    RootNode, ParentNode: TDOMElement;
    i: Integer;
    val: String;

begin
    try
        Doc := TXMLDocument.Create;

        // root 'loader' node
        RootNode := Doc.CreateElement('loader');
        RootNode.SetAttribute('version', '1.0');
        Doc.AppendChild(RootNode);

        RootNode := Doc.DocumentElement;

        // application node
        ParentNode := Doc.CreateElement('application');
        ParentNode.SetAttribute('id', targetProgName);
        RootNode.AppendChild(ParentNode);

        for i := Low(ArrayOfAttributes) to High(ArrayOfAttributes) do
        begin
            ParentNode := Doc.CreateElement(ArrayOfAttributes[i]);

            case ArrayOfAttributes[i] of
                'name': val := targetProgName;
                'description': val := targetProgDes;
                'version': val := targetProgVer;
                'vendor': val := targetProgVendor;
                'copyright': val := copyright;
            end;

            ParentNode.AppendChild(Doc.CreateTextNode(val));
            RootNode.ChildNodes.Item[0].AppendChild(ParentNode);
        end;

        ParentNode := Doc.CreateElement('fileset');
        ParentNode.SetAttribute('Java', '1.38');
        RootNode.ChildNodes.Item[0].AppendChild(ParentNode);
        
        ParentNode := Doc.CreateElement('files');
        for i := Low(inputFiles) to High(inputFiles) do
            ParentNode.AppendChild(Doc.CreateTextNode(inputFiles[i]));
        RootNode.ChildNodes.Item[0].ChildNodes.Item[5].AppendChild(ParentNode);

        WriteXMLFile(Doc, Format('%s/%s.alx', [outputPath, targetProgName]));
    finally
        Doc.Free;
    end;
end;

procedure WriteNewJAD;
var
    f: TextFile;
    fIn: File of LongInt;
    i: Integer;
    val: string;

begin
    AssignFile(f, Format('%s/%s.jad', [outputPath, targetProgName]));
    try
        ReWrite(f);
        for i := 0 to High(ArrayOfAttributes) do
        begin
            case ArrayOfAttributes[i] of
                'name': val := targetProgName;
                'description': val := targetProgDes;
                'version': val := targetProgVer;
                'vendor': val := targetProgVendor;
                'copyright': val := copyright;
            end;
            writeln(f, Format('MIDlet-%s: %s', [Capitalize(ArrayOfAttributes[i]), val]));
        end;

        for i := 0 to High(inputFiles) do
        begin
            AssignFile(fIn, inputFiles[i]);
            Reset(fIn);
            writeln(f, Format('RIM-COD-URL-%d: %s', [i, ExtractFileName(inputFiles[i])]));
            writeln(f, Format('RIM-COD-Size-%d: %d', [i, FileSize(fIn)]));
            Close(fIn);
        end;
        CloseFile(f);
    except
        on E: EInOutError do
        begin
            writeln('Unable to write to file: ', E.Message);
            halt(1);
        end;
    end;
end;

end.