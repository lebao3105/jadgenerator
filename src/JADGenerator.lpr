program JADGenerator;

{$mode objfpc}{$H+}

uses
	Classes, SysUtils, CustApp, Strutils,
	FileGen, General, FilesReader;

type
	TJADGenerator = class(TCustomApplication)
	private
		function GetRequiredOption(const short: Char; const long: String): String;
	protected
		procedure DoRun; override;
	public
		procedure WriteHelp; virtual;
	end;

{ TJADGenerator }

function TJADGenerator.GetRequiredOption(const short: Char; const long: String): String;
begin
	if not HasOption(short, long) then
		raise Exception.Create('Required argument not found: ' + short + '/' + long)
	else
		Result := GetOptionValue(short, long);
end;

procedure TJADGenerator.DoRun;
var
	ErrorMsg: String;
	ReadPath: String;
	ReadPathExt: String;

begin
	// stop program loop
	Terminate;

	// quick check parameters
	ErrorMsg := CheckOptions('han:i:o:v:d:e:r:', ['help', 'writeJAD', 'name:', 'input:', 'output:', 'version:', 'description:', 'vendor:', 'read:']);

	if ErrorMsg <> '' then begin
		ShowException(Exception.Create(ErrorMsg));
		Terminate;
		Exit;
	end;

	// parse parameters
	if HasOption('h', 'help') or (ParamCount = 0) then
	begin
		WriteHelp;
		Terminate;
		Exit;
	end;

	if HasOption('r', 'read') then
	begin
		writeln('Going to read the input file - all other options are ignored.');
		ReadPath := GetOptionValue('r', 'read');
		ReadPathExt := ExtractFileExt(ReadPath);

		case ReadPathExt of
			'.alx': begin
				writeln('Reading the file as an ALX...');
				ReadALX(ReadPath);
			end;
			'.jad': begin
				writeln('Reading the file as a JAD file...');
				ReadJAD(ReadPath);
			end;
		else
			raise Exception.Create('Unknown file extension!: ' + ReadPathExt);
		end;
	end
	else begin
		inputFiles := SplitString(GetRequiredOption('i', 'input'), ';');
		outputPath := GetRequiredOption('o', 'output');
		targetProgVer := GetRequiredOption('v', 'version');
		targetProgDes := GetRequiredOption('d', 'description');
		targetProgVendor := GetRequiredOption('e', 'vendor');
		targetProgName := GetRequiredOption('n', 'name');

		if FileExists(outputPath) then writeln('Warning: The file exists and will be overwritten by the program: ', outputPath);

		if HasOption('a', 'writeJAD') then
			WriteNewJAD
		else
			WriteNewALX;
	end;

end;

procedure TJADGenerator.WriteHelp;
begin
	writeln('Usage: ', ExeName, ' [args] [value]');
	writeln('Reads/generates JAD/ALX files, which are mainly used for BlackBerry OS.');
	writeln('Available flags:');
	writeln('')
end;

var
	Application: TJADGenerator;

begin
	Application := TJADGenerator.Create(nil);
	Application.Title := 'JAD generator for BlackBerry OS 7-';
	Application.Run;
	Application.Free;
end.

