## JADGenerator (name to change I guess)

A JAD+ALX generator/parser for BlackBerry OSes applications. Useful for quick installations without using a computer tool for a more portability (vnBBUtils somehow doesn't work on device dectection for me).

## Build

Install Free Pascal Compiler (fpc) and make.

Build:

```bash
$ make build
```

Install:

```bash
$ make install
```

Uninstall as well:

```bash
$ make uninstall
```

Clean:

```bash
$ make clean
```

You can build with Lazarus installed and added to `PATH`:

```bash
$ lazbuild Project.lpi
```

## Run

Run with no argument/`-h`/`--help` to show all options.

## Thanks to

* [Lunar Project](https://lunarproject.org) for reserving a lot of BlackBerry things

* [vnBBUtils](https://vnbb.bbvietnam.com) for the great and powerful tool

* BlackBerry for their devices, softwares