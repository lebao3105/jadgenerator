# Lazy-copied from my pasra1n with some modifications

# Compiler and its flags
C=fpc
# -Xs = auto strip at program compile time,
# -XX = smartlinking (ignore unneccessary code)
BUILD_FLAGS += -Xs -XX

# Windres, used to compile .rc file to .res
# x86_64-w64-mingw32-windres on Arch
# Now switched to fpcres - should come with FPC by default.
RCCOM = fpcres

# Install prefix and destination dir
PREFIX = /usr/local
DESTDIR =

ifeq ($(NO_SMARTLINK), 1)
	BUILD_FLAGS := $(filter-out -XX, $(BUILD_FLAGS))
endif

ifeq ($(NO_STRIP), 1)
	BUILD_FLAGS := $(filter-out -Xs, $(BUILD_FLAGS))
endif

JADGenerator:
	$(C) $(BUILD_FLAGS) src/JADGenerator.lpr -osrc/JADGenerator

clean: $(wildcard *.o) $(wildcard *.ppu) $(wildcard src/*.o) $(wildcard src/*.ppu)
	$(RM) $? src/*.rsj src/JADGenerator src/JADGenerator.exe src/*.a src/*.res
	$(RM) -rf lib backup src/backup src/lib

distclean: clean

install: JADGenerator
	install -D src/JADGenerator $(DESTDIR)$(PREFIX)/bin/JADGenerator

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/JADGenerator

all: JADGenerator

.PHONY: all JADGenerator install clean distclean uninstall